<?php
$header_root = 0;
$menu_pages = array(
	array(0, "index.php", "Inicio"),
	array(0, "nosotros.php", "Nosotros"),
	array(0, "servicios.php", "Servicios"),
	array(0, "portafolio.php", "Portafolio"),
	array(1, "contactanos.php", "Cont&aacute;ctanos")
);

include 'php/common/header.php';
?>

   <script type="text/javascript" src="https://cdn.emailjs.com/dist/email.min.js"></script>
   <script type="text/javascript">
      (function(){
         emailjs.init("user_GTL1u7XgYjxO2lI3d82tA");
      })();
   </script>
 
   <div class="gmap-area">
       <div class="container">
           <div class="row">
               <div class="col-sm-5 text-center">
                    <div class="gmap">
                      <img class="qrResponsive" src="images/privilegio/contactanos/qr_img.png" alt="Generador de C�digos QR Codes" />
<!-- 							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d975.2003684430306!2d-76.9826193!3d-12.1257318!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x16d0d28f1f151108!2sSynopsis!5e0!3m2!1ses!2spe!4v1434572147563" width="400" height="300" frameborder="0" style="border:0"></iframe> -->
					</div>
               </div>
			   <br/>
               <div class="col-sm-7 map-content">
                   <ul class="row">
                       <li class="col-sm-6">
                           <address>
                               <h5>Oficina Principal</h5>
                               <p>c/ San Antonio Este 629<br>
                               El R&iacute;mac, Lima</p>
                               <p>
                               <br/>
                               Email : <a href="mailto:AtencionCliente@privilegioperu.com">AtencionCliente@privilegioperu.com</a></p>
                           </address>
                       </li>
                   </ul>
                   <ul>
                       <li class="col-sm-4">
                           <img src="images/privilegio/contactanos/fb.png"/>&nbsp;&nbsp;<a target="_blank" href="https://facebook.com/privilegiosac" >/privilegiosac</a><br/><br/>
                       </li>
                       <li class="col-sm-6">
                           <img src="images/privilegio/contactanos/wpp.png"/>&nbsp;&nbsp;<a style="cursor: pointer" > (+51) 9887768081</a><br/><br/>
                       </li>
                   </ul>
               </div>
           </div>
       </div>
   </div>
   <br/><br/>

    <section id="contact-page">
        <div class="container">
            <div class="center" style="padding-bottom: 15px;">        
                <h2>D&eacute;janos un mensaje</h2>
                <p class="lead">Env&iacute;anos tus dudas y estaremos en contacto en el menor tiempo posible.</p>
            </div> 
            <div class="row contact-wrap"> 
                <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="">
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="form-group">
                            <label>Nombres&nbsp;</label><label style="color: red;">(*)</label>:
                            <input type="text" name="nombres" id="nombres" class="form-control" maxlength="30">
                        </div>
                        <div class="form-group">
                            <label>Email&nbsp;</label><label style="color: red;">(*)</label>:
                            <input type="email" name="correo" id="correo" class="form-control" maxlength="70">
                        </div>
                        <div class="form-group">
                            <label>Tel&eacute;fono&nbsp;</label><label style="color: red;">(*)</label>:
                            <input type="text" name="telefono" id="telefono" class="form-control" maxlength="10">
                        </div>
                        <div class="form-group">
                            <label>Nombre de compa&ntilde;&iacute;a:</label>
                            <input type="text" name="empresa" id="empresa" class="form-control" maxlength="100">
                        </div>                        
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Asunto&nbsp;</label><label style="color: red;">(*)</label>:
                            <input type="text" name="asunto" id="asunto" class="form-control" required="required" maxlength="80">
                        </div>
                        <div class="form-group">
                            <label>Mensaje&nbsp;</label><label style="color: red;">(*)</label>:
                            <textarea name="mensaje" id="mensaje" class="form-control" rows="10" required="required" maxlength="500"></textarea>
                        </div>     
                    </div>
                    <div class="col-sm-10 col-sm-offset-1">                   
                        <div class="form-group">
                            <button type="button" id="submit" class="btn btn-primary btn-lg">Enviar mensaje</button>
                        </div>
                        <div id="formAlert" class="alert alert-danger hide" role="alert">
                            Ocurri&oacute; un error!
                        </div>
                        <div id="formAlertOK" class="alert alert-success hide" role="alert">
                            OK!
                        </div>
                        <div id="formProcess" class="alert alert-info hide" role="alert">
                            Estamos procesando el mensaje ...
                        </div>
                    </div>
                </form> 
            </div>
        </div>
    </section>

    <?php include $root_dir.'php/common/footer.php';?>
   
    <script type="text/javascript">
    
        $("#submit").click(function() {
        	
        	if(validarDatos()){
        		$("#formProcess").removeClass("hide");
        		
              	emailjs.send("gmailPrivilegio","privilegiotemp",
              	{
              		     nombres:  $("#nombres").val(), 
              		     asunto:   $("#asunto").val(), 
              		     correo:   $("#correo").val(), 
              		     telefono: $("#telefono").val(), 
              		     empresa:  $("#empresa").val(), 
              		     mensaje:  $("#mensaje").val()
                }) 
              	.then(function(response) {
              		cleanFields();
            		$("#formProcess").addClass("hide");
            		$("#formAlertOK").html("El mensaje ha sido enviado satisfactoriamente.");
            		$("#formAlertOK").removeClass("hide");
              	   console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
              	}, function(err) {
            		$("#formProcess").addClass("hide");
            		$("#formAlert").html("Ha ocurrido un error al enviar el mensaje.");
            		$("#formAlert").removeClass("hide");
              	   console.log("FAILED. error=", err);
              	});
        	}
    	});
        
        function validarDatos(){
        	
        	if(!$("#formAlertOK").hasClass("hide")){
        		$("#formAlertOK").addClass("hide");
        	}
        	
            var nombres = $("#nombres").val().trim();
            var asunto  = $("#asunto").val().trim();
            var correo  = $("#correo").val().trim(); 
            var telefono= $("#telefono").val().trim(); 
            var empresa = $("#empresa").val().trim();
            var mensaje = $("#mensaje").val().trim();
            
            if(!notEmpty(nombres, "El campo 'Nombres' es un dato obligatorio.") ||
               !validateLongitud(nombres, 30, "El campo 'Nombres' excede la longitud permitida: 30.")){
               return false;
            }else if(!notEmpty(correo, "El campo 'Email' es un dato obligatorio.") ||
                     !validateLongitud(correo, 70, "El campo 'Email' excede la longitud permitida: 70.") || 
            		 !isEmail(correo, "Email con formato incorrecto.")){
               return false;
            }else if(!notEmpty(telefono, "El campo 'Tel&eacute;fono' es un dato obligatorio.") ||
                     !validateLongitud(telefono, 10, "El campo 'Tel&eacute;fono' excede la longitud permitida: 10.") || 
              	     !isNumeric(telefono, "Tel&eacute;fono con formato incorrecto. Solamente se debe ingresar n&uacute;meros.")){
                 return false;
            }else if(!notEmpty(asunto, "El campo 'Asunto' es un dato obligatorio.") ||
                     !validateLongitud(asunto, 80, "El campo 'Asunto' excede la longitud permitida: 80.")){
               return false;
            }else if(!notEmpty(mensaje, "El campo 'Mensaje' es un dato obligatorio.")||
                     !validateLongitud(mensaje, 500, "El campo 'Mensaje' excede la longitud permitida: 500.")){
               return false;
            }else{
               $("#formAlert").addClass("hide");
               return true;
            }
		     
        }

        function cleanFields(){
            $("#nombres").val("");
            $("#asunto").val("");
            $("#correo").val(""); 
            $("#telefono").val(""); 
            $("#empresa").val("");
            $("#mensaje").val("");
        }
        
        function notEmpty(campo, mensaje){
        	if(campo==''){	
        		$("#formAlert").html(mensaje);
        		$("#formAlert").removeClass("hide");
        		return false;
        	}
    		return true;
        }
        
        function isNumeric(campo, mensaje){
        	var reg = /^\d+$/;
        	if(!reg.test(campo)){	
        		$("#formAlert").html(mensaje);
        		$("#formAlert").removeClass("hide");
        		return false;
        	}
    		return true;
        }
        
        function isEmail(campo, mensaje){
            var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        	if(!reg.test(campo.toLowerCase())){	
        		$("#formAlert").html(mensaje);
        		$("#formAlert").removeClass("hide");
        		return false;
        	}
    		return true;
        }

        function validateLongitud(campo, longitud, mensaje){
        	if(campo.length >longitud){	
        		$("#formAlert").html(mensaje);
        		$("#formAlert").removeClass("hide");
        		return false;
        	}
    		return true;
        }
    </script>
   
</body>
</html>