<?php
$menu_footer = array(
   array("index.php", "Inicio"),
   array("nosotros.php", "Nosotros"),
   array("servicios.php", "Servicios"),
   array("portafolio.php", "Portafolio"),
   array("contactanos.php", "Cont&aacute;ctanos")
);
?>

   <!-- footer-->
   <section id="bottom">
      <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
         <div class="row">
            <div class="col-md-3 col-sm-6">
               <div class="widget">
                  <h3>Empresa</h3>
                  <ul>
                      <?php
                      foreach($menu_footer as $s){
                          echo "<li><a href='" . $root_dir . $s[0] . "'>" . $s[1] . "</a>"; 
                      }
                      ?> 
                  </ul>
               </div>
            </div>

            <div class="col-md-3 col-sm-6">
               <div class="widget">
                  <h3>Servicios</h3>
                  <ul>
                     <li><a>Confecci&oacute;n</a></li>
                     <li><a>Reparaci&oacute;n</a></li>
                     <li><a>Venta de Ropa</a></li>
                     <li><a>Dise&ntilde;o de Moda</a></li>
                     <li><a>Asesor&iacute;a de Moda</a></li>
                  </ul>
               </div>
            </div>

            <div class="col-md-3 col-sm-6">
               <div class="widget">
                  <h3>Productos</h3>
                  <ul>
                     <li><a href="#">Camisas</a></li>
                     <li><a href="#">Polos Cuello V</a></li>
                     <li><a href="#">Polos Cuello Camisero</a></li>
                     <li><a href="#">Polos Cuello Redondo</a></li>
                     <li><a href="#">Pantalones</a></li>
                  </ul>
               </div>
            </div>
            <!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
               <div class="widget">
                  <h3>Proveedores</h3>
                  <ul>
                     <li><a href="#">Xiomi Per&uacute;</a></li>
                     <li><a href="#"></a></li>
                     <li><a href="#"></a></li>
                     <li><a href="#"></a></li>
                     <li><a href="#"></a></li>
                     <li><a href="#"></a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <a class="btn btn-default btn-circle"><i class="fa fa-angle-up"></i></a>
   </section>

   <footer id="footer" class="midnight-blue">
      <div class="container">
         <div class="row">
            <div class="col-sm-6">
               &copy; 2018 <a target="_blank" href="javascript:scrollToElement();" title="Privilegio">Privilegio SAC</a>. All Rights Reserved.
            </div>
            <div class="col-sm-6">
               <ul class="pull-right">
                  <?php
                  foreach($menu_footer as $s){
                      echo "<li><a href='" . $root_dir . $s[0] . "'>" . $s[1] . "</a>"; 
                  }
                  ?> 
               </ul>
            </div>
         </div>
      </div>
   </footer>
   <!--/#footer-->

   <script src="<?php echo $root_dir; ?>js/bootstrap.min.js"></script>
   <script src="<?php echo $root_dir; ?>js/jquery.prettyPhoto.js"></script>
   <script src="<?php echo $root_dir; ?>js/main.js"></script>
   <script src="<?php echo $root_dir; ?>js/wow.min.js"></script>
   