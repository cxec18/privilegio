<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Nuestra mejor presentaci&oacute;n es virtual">
    <meta name="author" content="Carlos Effio">
   
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Privilegio SAC" />
    <meta property="og:description" content="Nuestra mejor presentaci&oacute;n es virtual" />
    <meta property="og:image" content="http://privilegioperusac.unaux.com/images/custom/privilegioFondo.png" />

   <title>Privilegio SAC</title>
   
   <?php
       $root_dir = "";
       for($x=0; $x<$header_root; $x++){
           $root_dir = "../" . $root_dir;
       }
   ?>
       
   <!-- core CSS -->
   <link href="<?php echo $root_dir; ?>css/bootstrap.min.css" rel="stylesheet">
   <link href="<?php echo $root_dir; ?>css/font-awesome.min.css" rel="stylesheet">
   <link href="<?php echo $root_dir; ?>css/animate.min.css" rel="stylesheet">
   <link href="<?php echo $root_dir; ?>css/prettyPhoto.css" rel="stylesheet">
   <link href="<?php echo $root_dir; ?>css/main.css" rel="stylesheet">
   <link href="<?php echo $root_dir; ?>css/style.css" rel="stylesheet">
   <link href="<?php echo $root_dir; ?>css/responsive.css" rel="stylesheet"> 
    
   <!--[if lt IE 9]>
       <script src="<?php echo $root_dir; ?>js/html5shiv.js"></script>
       <script src="<?php echo $root_dir; ?>js/respond.min.js"></script>
       <![endif]
   -->
   
   <link rel="shortcut icon" href="<?php echo $root_dir; ?>images/ico/privilegio-icono.ico">
   <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $root_dir; ?>images/ico/apple-touch-icon-144-precomposed.png">
   <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $root_dir; ?>images/ico/apple-touch-icon-114-precomposed.png">
   <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $root_dir; ?>images/ico/apple-touch-icon-72-precomposed.png">
   <link rel="apple-touch-icon-precomposed" href="<?php echo $root_dir; ?>images/ico/apple-touch-icon-57-precomposed.png">
      
   <script src="<?php echo $root_dir; ?>js/jquery.js"></script>

</head>

<body class="homepage">
   <div class="top-bar">
      <div class="container">
         <div class="row">
            <div class="span7">
               <small>Email : <a href="mailto:AtencionCliente@privilegioperu.com">AtencionCliente@privilegioperu.com</a> | Tel : <a style="cursor: pointer">(+51) 987768081</a></small>
            </div>
         </div>
      </div>
   </div>

   <header id="nav-wrapper">
      <nav id="nav" class="navbar navbar-inverse" role="banner">
         <div class="container">
            <div class="navbar-header">
               <button type="button" id="buttonResponsive" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span> 
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" id="logoResponsive" href="<?php echo $root_dir; ?>index.php"><img id="logo" src="<?php echo $root_dir; ?>images/custom/privilegio-v2.png" alt="logo"></a>
            </div>

            <div class="collapse navbar-collapse navbar-right">
               <ul class="nav navbar-nav">
               		
           			<?php
					foreach($menu_pages as $s){
						if ($s[0] > 0) { 
						    echo "<li class='active'><a href='" . $root_dir . $s[1] . "'>" . $s[2] . "</a>"; 
						}else{
						    echo "<li><a href='" . $root_dir . $s[1] . "'>" . $s[2] . "</a>"; 
						}
					}
					?>	
               </ul>
            </div>
         </div>
      </nav>
   </header>
