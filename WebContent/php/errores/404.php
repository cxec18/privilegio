<?php
$header_root = 2;
$menu_pages = array(
	array(0, "index.php", "Inicio"),
	array(0, "nosotros.php", "Nosotros"),
	array(0, "servicios.php", "Servicios"),
	array(0, "portafolio.php", "Portafolio"),
	array(0, "contactanos.php", "Cont&aacute;ctanos")
);

include '../../php/common/header.php';
?>

<section id="error" class="container text-center">
  <h1>404, P&aacute;gina no encontrada.</h1>
  <p>La p&aacute;gina que est&aacute;s buscando no existe o ha ocurrido un error.</p>
  <a class="btn btn-primary" href="<?php echo $root_dir.'index.php';?>">Regresar a la p&aacute;gina inicial</a>
</section>


<?php include $root_dir.'php/common/footer.php';?>

</body>
</html>