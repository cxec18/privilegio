<?php
$header_root = 2;
$menu_pages = array(
   array(1, "portafolio.php", "Regresar")
);

include '../../php/common/header.php';

$productos_img = 'images/privilegio/productos/';
?>
    
    <link href="<?php echo $root_dir; ?>util/carusel-prod/css/bootstrap-carusel.css" rel="stylesheet">  
    <link href="<?php echo $root_dir; ?>util/carusel-prod/css/full-slider.css" rel="stylesheet">    
   
    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active" style="background-image: url('<?php echo $root_dir . $productos_img; ?>A0001/IMG_3840.png')">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
          <div class="carousel-item" style="background-image: url('<?php echo $root_dir . $productos_img; ?>A0001/IMG_3779.png')">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
          <!-- 
          <div class="carousel-item" style="background-image: url('<?php echo $root_dir . $productos_img; ?>A0001/IMG_3768.png')">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div> -->
          <div class="carousel-item" style="background-image: url('<?php echo $root_dir . $productos_img; ?>A0001/IMG_3742.png')">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
          <div class="carousel-item" style="background-image: url('<?php echo $root_dir . $productos_img; ?>A0001/IMG_3932.png')">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->
    <section class="py-5">
      <div class="container">
        <h1>A0001 - Camisa algod&oacute;n strech</h1>
        <p>Combinamos nuestras mejores habilidades para el cumplimiento de los m&aacute;s altos est&aacute;ndares 
           de calidad en nuestra camisa de Algod&oacute;n Strech, color rosa beb&eacute;, en tallas S y M (Slim Fit). 
           Fabricaci&oacute;n 100% peruana. <br/><br/>
            - Con colaboraci&oacute;n especial, modelo: <strong>Jose Luis Bueno Abanto</strong>
        </p>
      </div>
    </section>
   
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.12&appId=841972635821178&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    
    <section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-8">
                    <div class="media contact-info wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        
                        <div width="100%" class="fb-comments" data-href="https://www.facebook.com/privilegiosac/posts/224554338089341" data-numposts="10"></div>
                        
                    </div>
                </div>
            </div>
        </div>  
   </section>
   <?php include $root_dir.'php/common/footer.php';?>

   <script src="<?php echo $root_dir; ?>util/carusel-prod/bootstrap.bundle.js"></script>
    
</body>
</html>