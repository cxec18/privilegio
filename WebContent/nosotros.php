<?php
$header_root = 0;
$menu_pages = array(
	array(0, "index.php", "Inicio"),
	array(1, "nosotros.php", "Nosotros"),
	array(0, "servicios.php", "Servicios"),
	array(0, "portafolio.php", "Portafolio"),
	array(0, "contactanos.php", "Cont&aacute;ctanos")
);

include 'php/common/header.php';
?>
        
    <link href="css/jquery.bxslider.css" rel="stylesheet">    
    
	    <section id="services" class="service-item">
		   <div class="container">
	            <div class="center wow fadeInDown">
	                <h2>Misi&oacute;n</h2>
	                <p class="lead">
	                	Somos un equipo emprendedor	que proporciona outfit de alta calidad al mercado peruano,<br/> 
	                	poniendo al servicio del cliente nuestros variados canales de atenci&oacute;n con la finalidad de preservar su satisfacci&oacute;n,<br/> 
	                	y a sus empleados la posibilidad de desarrollar valores empresariales siendo nuestro activo m&aacute;s valioso.
	                </p>
	            </div>
				<div class="center wow fadeInDown">
	                <h2>Visi&oacute;n</h2>
	                <p class="lead">
	                	Ser reconocidos como empresa l&iacute;der en la venta outfit de alta calidad a nivel del Per&uacute;, <br/> 
	                	mantener nuestros altos est&aacute;ndares de producci&oacute;n con el precio justo y accesible <br/> 
                        con el que cautivamos inicialmente a nuestros clientes,<br/> 
                        ampliar las oportunidades de desarrollo tanto personal como profesional a nuestros empleados, <br/> 
                        y contribuir de forma positiva a la sociedad.
	               	</p>
	            </div>
	        </div>
	    </section>
                
   
       <section id="content">
          <div class="container">
             <div class="row">
                <div class="col-xs-12 col-sm-8 wow fadeInDown">
                   <div class="tab-wrap">
                      <div class="media">
                         <div class="parrent pull-left">
                            <ul class="nav nav-tabs nav-stacked">
                               <li class=""><a href="#tab1" data-toggle="tab" class="analistic-01">Nuestra historia</a></li>
                               <li class="active"><a href="#tab2" data-toggle="tab" class="analistic-02">Nuestra filosof&iacute;a</a></li>
                               <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">&iquest;Qu&eacute; hacemos?</a></li>
                               <li class=""><a href="#tab4" data-toggle="tab" class="tehnical">Trabaja con nosotros</a></li>
                            </ul>
                         </div>
    
                         <div class="parrent media-body">
                            <div class="tab-content">
                               <div class="tab-pane fade" id="tab1">
                                <h2>&Eacute;rase una mala experiencia ...</h2>
                                <p>Privilegio llega con un nuevo concepto de venta que nace a partir de nuestra experiencia
                                   como j&oacute;venes consumidores de los mejores productos provenientes de reconocidas 
                                   marcas a nivel nacional e internacional. <ins>Recopilamos las mejores experiencias y sinsabores </ins>
                                   durante nuestras compras, y decidimos que era momento de poner punto y aparte al dise&ntilde;o actual,
                                   y hacer evolucionar el estilo.</p>
                               </div>
    
                               <div class="tab-pane fade active in" id="tab2">
                                <h2>Dar es mejor que recibir.</h2>
                                <p>Sab&iacute;amos bien lo que nosotros los consumidores nos merecemos, y nos dimos la libertad de 
                                   iniciar y compartir con todo el p&uacute;blico este proyecto, el cual se rige bajo el pensamiento: 
                                   <i>&quot;dar es mejor que recibir&quot;</i>. Que el significado que le otorgamos en el contexto de ventas es 
                                   el de antes de pensar si quiera en nuestra actividad lucrativa <strong>(RECIBIR)</strong>, planifiquemos 
                                   cada producto pensando en qu&eacute; es realmente lo que desean y merecen nuestros clientes, y 
                                   trazarnos el objetivo de explotar nuestras m&aacute;ximas capacidades para ofrecerles ese producto ideal 
                                   <strong>(DAR)</strong>.</p>
                               </div>
    
                               <div class="tab-pane fade" id="tab3">
                                  <h2>Nuestra experiencia en tus manos.</h2>
                                  <p>La labor que realizamos es la de poner a disposici&oacute;n del mercado el mejor Outfit 
                                     <strong>dise&ntilde;ado y creado en Per&uacute;</strong>, con la finalidad de satisfacer 
                                     los distintos y exigentes gustos masculinos.</p>
                               </div>
    
                               <div class="tab-pane fade" id="tab4">
                                  <div class="media">
                                     <div class="pull-left">
                                        <img class="img-responsive" src="images/tab1.png">
                                     </div> 
                                     <div class="media-body">
                                        <h2>Nuestro activo m&aacute;s importante.</h2>
                                        <p>Todos somos vendedores por excelencia; vendemos ideas, servicios o lo m&aacute;s valioso que 
                                           tenemos, nuestro tiempo. Cont&aacute;ctate con nosotros para averiguar c&oacute;mo puedes ser parte 
                                           de &eacute;ste gran grupo humano, mejorar tus habilidades en ventas, y generar un 
                                           aporte a nuestro crecimiento con tu talento.</p>
                                     </div>
                                  </div>
                               </div>
                               
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
    
                <div class="col-xs-12 col-sm-4 wow fadeInDown">
                   <div class="testimonial">
                      <h2>Testimonios</h2>
                      <div class="media testimonial-inner">
                         <div class="pull-left">
                            <img class="img-responsive img-circle" src="images/joseluis.png">
                         </div>
                         <div class="media-body">
                            <p>La textura de la camisa (A0001) es muy c&oacute;moda, y lo mejor de todo es que no da calor a pesar de ser manga larga.</p>
                            <span><strong>- Jos&eacute; Luis/</strong> Modelo</span>
                         </div>
                      </div>
                        
                      <!-- 
                      <div class="media testimonial-inner">
                         <div class="pull-left">
                            <img class="img-responsive img-circle" src="images/testimonials1.png">
                         </div>
                         <div class="media-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                            <span><strong>- John Doe/</strong> Director of corlate.com</span>
                         </div>
                      </div>
                       -->
                       
                   </div>
                </div>
             </div>
          </div>
       </section>
                
        <section id="conatcat-info">
            <div class="container">
               
                <!-- our-team -->
                <div class="team">
                    <div class="center wow fadeInDown">
                        <h2>Equipo Privilegio</h2>
                        <p class="lead">
                           "Puedes dise&ntilde;ar y crear, y construir el lugar m&aacute;s maravilloso del mundo, <br/> 
                            pero se necesita gente para hacer el sue&ntilde;o realidad"<br/>
                            <strong>Walt Disney</strong></p>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-4 col-sm-6 col-md-offset-1">
                            <div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <div class="media">
                                    <div class="pull-left">
                                        <a href="#"><img class="media-object" src="images/privilegio/nosotros/fotos/carlos.png" alt=""></a>
                                    </div>
                                    <div class="media-body">
                                        <h4>Carlos Effio</h4>
                                        <h5>Gerente General</h5>
                                        <ul class="tag clearfix">
                                            <li class="btn"><a>Tecnolog&iacute;a</a></li>
                                            <li class="btn"><a>Dise&ntilde;o</a></li>
                                            <li class="btn"><a>Ventas</a></li>
                                        </ul>
                                        <ul class="social_icons">
                                            <li><a href="https://www.facebook.com/cxec20" target="_blank">
                                                <i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li><a href="https://www.pinterest.es/cxec20/" target="_blank">
                                                <i class="fa fa-pinterest"></i></a>
                                            </li>
                                            <li><a href="https://www.linkedin.com/in/cxec20/" target="_blank">
                                                <i class="fa fa-linkedin"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--/.media -->
                                <p><br/>La calidad es el resultado del esfuerzo<br/> de la inteligencia.</p>
                            </div>
                        </div>
                        <!--/.col-lg-4 -->
                        <div class="col-md-4 col-sm-6 col-md-offset-2">
                            <div class="single-profile-top wow fadeInDown" data-wow-duration="1000ms"
                            data-wow-delay="300ms">
                                <div class="media">
                                    <div class="pull-left">
                                        <a href="#"><img class="media-object" src="images/privilegio/nosotros/fotos/Julio.png" alt=""></a>
                                    </div>
                                    <div class="media-body">
                                        <h4>Julio Cordova</h4>
                                        <h5>Gerente TI</h5>
                                        <ul class="tag clearfix">
                                            <li class="btn"><a>Tecnolog&iacute;a</a></li>
                                            <li class="btn"><a>Ventas</a></li>
                                        </ul>
                                        <ul class="social_icons">
                                            <li><a href="https://www.facebook.com/julio.andre92" target="_blank">
                                                <i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li><a href="https://www.pinterest.es/julioandre/" target="_blank">
                                                <i class="fa fa-pinterest"></i></a>
                                            </li>
                                            <li><a href="https://www.linkedin.com/in/julioandre92/" target="_blank">
                                                <i class="fa fa-linkedin"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--/.media -->
                                <p><br/>La perseverancia e innovaci&oacute;n son nuestras bases para el desarrollo diario.</p>
                            </div>
                        </div>
                        <!--/.col-lg-4 -->
                    </div>
                    <!--/.row -->
                    <div class="row team-bar">
                        <div class="first-one-arrow hidden-xs">
                            <hr>
                        </div>
                        <div class="first-arrow hidden-xs">
                            <hr>
                            <i class="fa fa-angle-up"></i>
                        </div>
                        <div class="second-arrow hidden-xs">
                            <hr>
                            <i class="fa fa-angle-down"></i>
                        </div>
                        <div class="third-arrow hidden-xs">
                            <hr>
                            <i class="fa fa-angle-up"></i>
                        </div>
                        <div class="fourth-arrow hidden-xs">
                            <hr>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </div>
                    <!--skill_border-->
                    <div class="row clearfix">
                        <div class="col-md-4 col-sm-6 col-md-offset-4">
                            <div class="single-profile-bottom wow fadeInUp" data-wow-duration="1000ms"
                            data-wow-delay="600ms">
                                <div class="media">
                                    <div class="pull-left">
                                        <a href="#"><img class="media-object" src="images/privilegio/nosotros/fotos/John.png" alt=""></a>
                                    </div>
                                    <div class="media-body">
                                        <h4>John Ortiz</h4>
                                        <h5>Sub Gerente </h5>
                                        <ul class="tag clearfix">
                                            <li class="btn"><a>Dise&ntilde;o</a></li>
                                            <li class="btn"><a>Ventas</a></li>
                                        </ul>
                                        <ul class="social_icons">
                                            <li><a href="https://www.facebook.com/john.ortiz.33046" target="_blank">
                                                <i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li><a href="https://www.pinterest.es/jortizr/" target="_blank">
                                                <i class="fa fa-pinterest"></i></a>
                                            </li>
                                            <li><a href="https://www.linkedin.com/in/john-ortiz-99726735/" target="_blank">
                                                <i class="fa fa-linkedin"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--/.media -->
                                <p><br/>Enfrentarse a la vida con determinaci&oacute;n y estilo da los mejores resultados.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
   <!-- 
    <section id="partner">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Nuestros Proveedores</h2>
                <p class="lead">Brindamos soluciones integrales hechas a la medida para empresas de diversos sectores del pa&iacute;s y el extranjero,<br/>
                  				especialmente en los rubros de Banca y Seguros, Distribuci&oacute;n, Pesca, Retail, Miner&iacute;a y Manufactura.</p>
            </div>    

			<div class="slider1">
			  <div class="slide"><img src="images/partners/partner1.png"></div>
			  <div class="slide"><img src="images/partners/partner2.png"></div>
			  <div class="slide"><img src="images/partners/partner3.png"></div>
			  <div class="slide"><img src="images/partners/partner4.png"></div>
			  <div class="slide"><img src="images/partners/partner5.png"></div>
			</div>
        </div>
    </section>        
    -->
         
   <?php include $root_dir.'php/common/footer.php';?>
   
   <script src="js/jquery.mobile.custom.min.js"></script>
   <script src="js/jquery.bxslider.min.js"></script>
   
   <script type="text/javascript">
      $(document).ready(function(){
        $('.slider1').bxSlider({  
         auto: true,
          slideWidth: "auto",
          minSlides: 1,
          maxSlides: 5,
          pause: 3000,
          slideMargin: 10,
          pager: true,
          prevText: 'Anterior',
          nextText: 'Siguiente'
        });
      });
   </script>
    
</body>
</html>