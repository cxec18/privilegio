<?php
$menu_pages = array(
	array(1, "index.php", "Inicio"),
	array(0, "nosotros.php", "Nosotros"),
	array(0, "servicios.php", "Servicios"),
	array(0, "portafolio.php", "Portafolio"),
	array(0, "contactanos.php", "Cont&aacute;ctanos")
);

include 'common/header.php';
?>

 <script type="text/javascript">

  (function(d, s, id){
	   var js, fjs = d.getElementsByTagName(s)[0];
	   if (d.getElementById(id)) {return;}
	   js = d.createElement(s); js.id = id;
	   js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";
	   fjs.parentNode.insertBefore(js, fjs);
	 }(document, 'script', 'Messenger'));
  </script>
	  
   <script src="bxslider-4-4.2.12/dist/jquery.bxslider.js"></script>
   
   <script type="text/javascript">
     $(document).ready(function(){
        $('.bxslider').bxSlider({
           mode: 'fade',
           captions: true
        });
    });
   </script>

   <section id="middle">
      <div class="container">
         <div class="row">
            <div class="col-sm-6 wow fadeInDown">
               <div class="skill">
                  <h2>Nuestros Skills</h2>
                  <p>Combinamos nuestras mejores habilidades para el cumplimiento de los m&aacute;s
                     altos est&aacute;ndares de producci&oacute;n textil que podamos ofrecer a nuestros clientes,
                     sin impactar su econom&iacute;a.</p>
                  <p>Nuestros skills est&aacute;n basados en nuestra experiencia como consumidores de los mejores
                     productos de reconocidas marcas a nivel nacional e internacional, y la colaboraci&oacute;n 
                     de nuestros provedores altamente capacitados.</p>

                  <div class="progress-wrap">
                     <h3>Dise&ntilde;o</h3>
                     <div class="progress">
                        <div class="progress-bar  color1" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                           <span class="bar-width">95%</span>
                        </div>
                     </div>
                  </div>
                  <div class="progress-wrap">
                     <h3>Calidad</h3>
                     <div class="progress">
                        <div class="progress-bar color4" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                           <span class="bar-width">95%</span>
                        </div>
                     </div>
                  </div>
                  <div class="progress-wrap">
                     <h3>Confortabilidad</h3>
                     <div class="progress">
                        <div class="progress-bar color3" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                           <span class="bar-width">90%</span>
                        </div>
                     </div>
                  </div>
                  <div class="progress-wrap">
                     <h3>Precio</h3>
                     <div class="progress">
                        <div class="progress-bar color2" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                           <span class="bar-width">90%</span>
                        </div>
                     </div>
                  </div>
               </div>
               
               <br/><br/>
            </div>

            <div class="col-sm-6 wow fadeInDown">
               <div class="accordion">
                  <div class="panel-group" id="accordion1">
                     <div class="bxslider">
                       <div><img src="images/privilegio/productos/A0001/IMG_3779.png" title="Camisa A0001: El&aacute;stico confortable - S/ 89"></div>
                       <div><img src="images/privilegio/productos/A0001/IMG_3932.png" title="Camisa A0001: El&aacute;stico confortable - S/ 89"></div>
                     </div>
                  </div>
               </div>
            </div>
			
         </div>
      </div>
   </section>

   <section id="content">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-8 wow fadeInDown">
               <div class="tab-wrap">
                  <div class="media">
                     <div class="parrent pull-left">
                        <ul class="nav nav-tabs nav-stacked">
                           <li class=""><a href="#tab1" data-toggle="tab" class="analistic-01">Responsive Web Design</a></li>
                           <li class="active"><a href="#tab2" data-toggle="tab" class="analistic-02">Premium Plugin Included</a></li>
                           <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Predefine Layout</a></li>
                           <li class=""><a href="#tab4" data-toggle="tab" class="tehnical">Our Philosopy</a></li>
                           <li class=""><a href="#tab5" data-toggle="tab" class="tehnical">What We Do?</a></li>
                        </ul>
                     </div>

                     <div class="parrent media-body">
                        <div class="tab-content">
                           <div class="tab-pane fade" id="tab1">
                              <div class="media">
                                 <div class="pull-left">
                                    <img class="img-responsive" src="images/tab2.png">
                                 </div>
                                 <div class="media-body">
                                    <h2>Adipisicing elit</h2>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use.</p>
                                 </div>
                              </div>
                           </div>

                           <div class="tab-pane fade active in" id="tab2">
                              <div class="media">
                                 <div class="pull-left">
                                    <img class="img-responsive" src="images/tab1.png">
                                 </div>
                                 <div class="media-body">
                                    <h2>Adipisicing elit</h2>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use.</p>
                                 </div>
                              </div>
                           </div>

                           <div class="tab-pane fade" id="tab3">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                 reprehenderit.</p>
                           </div>

                           <div class="tab-pane fade" id="tab4">
                              <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need
                                 to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words</p>
                           </div>

                           <div class="tab-pane fade" id="tab5">
                              <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need
                                 to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin
                                 words, combined with a handful of model sentence structures,</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col-xs-12 col-sm-4 wow fadeInDown">
               <div class="testimonial">
                  <h2>Testimonios</h2>
                  <div class="media testimonial-inner">
                     <div class="pull-left">
                        <img class="img-responsive img-circle" src="images/testimonials1.png">
                     </div>
                     <div class="media-body">
                        <p>La textura de la camisa (A0001) es muy c&oacute;moda, y lo mejor de todo es que no da calor a pesar de ser manga larga.</p>
                        <span><strong>-Jos&eacute; Luis/</strong> Director of corlate.com</span>
                     </div>
                  </div>

                  <div class="media testimonial-inner">
                     <div class="pull-left">
                        <img class="img-responsive img-circle" src="images/testimonials1.png">
                     </div>
                     <div class="media-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                        <span><strong>-John Doe/</strong> Director of corlate.com</span>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>

<?php include 'common/footer.php';?>