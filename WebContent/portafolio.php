<?php
$header_root = 0;
$menu_pages = array(
	array(0, "index.php", "Inicio"),
	array(0, "nosotros.php", "Nosotros"),
	array(0, "servicios.php", "Servicios"),
	array(1, "portafolio.php", "Portafolio"),
	array(0, "contactanos.php", "Cont&aacute;ctanos")
);

include 'php/common/header.php';

$productos_img = 'images/privilegio/productos/';
$productos_root = 'php/productos/';
?>
  
    <script src="js/angular.min.js"></script>
    
    <section id="portfolio" data-ng-app="myApp">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Productos Recientes</h2>
                <p class="lead">Permanece actualizado de nuestra variedad productos. </p>
            </div>
            <ul class="portfolio-filter text-center" data-ng-controller="portafolioCtrl">
                <li><a class="btn btn-default active" href="#" data-filter="*">Todos</a></li>
                <li data-ng-repeat="x in tipoProductos" my-repeat-directive>
                	<a class="btn btn-default" href="#" data-filter=".{{ x.filtro }}">{{ x.descripcion }}</a>
                </li>
            </ul>

            <div class="row">
                <div class="portfolio-items" data-ng-controller="productosCtrl">
                    
                    <div class="portfolio-item {{ y.filtro }} col-xs-12 col-sm-4 col-md-3" data-ng-repeat="y in productos">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" data-ng-src="<?php echo $productos_img ?>{{ y.imagen }}" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a class="preview" href="<?php echo $productos_root ?>{{ y.enlace }}" rel="prettyPhoto">{{ y.titulo }}</a></h3>
                                    <p>{{ y.descripcion }}</p>
                                    <a class="preview" href="<?php echo $productos_root ?>{{ y.enlace }}"><i class="fa fa-eye"></i> Visualizar</a>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
       
   <script src="js/jquery.isotope.min.js"></script>
   
   <script type="text/javascript">
   
       var app = angular.module('myApp', [])
         .directive('myRepeatDirective', function() {
           return function(scope, element, attrs) {
		       if (scope.$last){
		    	   setTimeout(function(){ 
			    	   doIsotope();
		    	   }, 1000);
		       }
           }
         });
        
       app.controller('portafolioCtrl', function($scope, $http) {
    	   $http.get("json/sqlTipoProducto.php")
           .then(function (response) {
              $scope.tipoProductos = response.data.records;
           });
       });
       app.controller('productosCtrl', function($scope, $http) {
    	   $http.get("json/sqlProducto.php")
           .then(function (response) {
              $scope.productos = response.data.records;
           });
       });
      
    
   	function doIsotope(){
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
   	}

   </script>
      
   <?php include 'php/common/footer.php';?>
   
</body>
</html>
