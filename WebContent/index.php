<?php
$header_root = 0;
$menu_pages = array(
	array(1, "index.php", "Inicio"),
	array(0, "nosotros.php", "Nosotros"),
	array(0, "servicios.php", "Servicios"),
	array(0, "portafolio.php", "Portafolio"),
	array(0, "contactanos.php", "Cont&aacute;ctanos")
);

include 'php/common/header.php';
?>

   
    <link href="<?php echo $root_dir; ?>util/carusel-prod/css/bootstrap-carusel.css" rel="stylesheet">  
    <link href="<?php echo $root_dir; ?>util/carusel-prod/css/full-slider.css" rel="stylesheet">    
    
    <!-- 
   <link href="util/bxslider-4.2.12/jquery.bxslider.css" rel="stylesheet">
   <script src="util/bxslider-4.2.12/jquery.bxslider.js"></script>
   
   <script type="text/javascript">
     $(document).ready(function(){
        $('.bxslider').bxSlider({
           mode: 'fade',
           captions: true
        });
    });
   </script>
    -->
           
    <header>
      <div id="carouselExampleIndicators" class="carousel slide">
        <div class="carousel-inner" role="listbox">
          <div id="imgResponsive" class=" carousel-item active" style="background-image: url('<?php echo $root_dir . $productos_img; ?>images/privilegio/FondoPrincipal.png');">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
        </div>
      </div>
    </header>
       
    <style>
    .mobile{
        height: 100% !important;
    }
    </style>
    

    <script type="text/javascript">
       function resize() {
           if ($(window).width() < 514) {
              $('#imgResponsive').addClass('mobile');
           } else {
              $('#imgResponsive').removeClass('mobile');
           }
       }
   
       $(document).ready( function() {
           $(window).resize(resize);
           resize();
       });
    </script>           
                
                
   <!-- 
   <section id="middle">
      <div class="container">
         <div class="row">
            <div class="col-sm-6 wow fadeInDown">
               <div class="skill">
                  <h2>La calidad es el resultado del esfuerzo<br/> de la inteligencia.</h2>
                  <p>Combinamos nuestras mejores habilidades para el cumplimiento de los m&aacute;s
                     altos est&aacute;ndares de producci&oacute;n que podamos ofrecer a nuestros clientes,
                     sin impactar su econom&iacute;a.</p>
                  <p>Nuestros skills est&aacute;n basados en nuestra experiencia como consumidores de los mejores
                     productos de reconocidas marcas a nivel nacional e internacional, y la colaboraci&oacute;n 
                     de nuestros provedores altamente capacitados.</p>

                  <div class="progress-wrap">
                     <h3>Dise&ntilde;o</h3>
                     <div class="progress">
                        <div class="progress-bar  color1" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                           <span class="bar-width">95%</span>
                        </div>
                     </div>
                  </div>
                  <div class="progress-wrap">
                     <h3>Calidad</h3>
                     <div class="progress">
                        <div class="progress-bar color4" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                           <span class="bar-width">95%</span>
                        </div>
                     </div>
                  </div>
                  <div class="progress-wrap">
                     <h3>Confortabilidad</h3>
                     <div class="progress">
                        <div class="progress-bar color3" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                           <span class="bar-width">90%</span>
                        </div>
                     </div>
                  </div>
                  <div class="progress-wrap">
                     <h3>Precio</h3>
                     <div class="progress">
                        <div class="progress-bar color2" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                           <span class="bar-width">90%</span>
                        </div>
                     </div>
                  </div>
               </div>
               
               <br/><br/>
            </div>

            <div class="col-sm-6 wow fadeInDown">
               <div class="accordion">
                  <div class="panel-group" id="accordion1">
                     <div class="bxslider">
                       <div><img src="images/privilegio/personaje.png" title="askas"></div>
                       <div><img src="images/privilegio/productos/A0001/IMG_3779-V.png" title="Camisa A0001: El&aacute;stico confortable - S/ 89"></div>
                       <div><img src="images/privilegio/productos/A0001/IMG_3932-V.png" title="Camisa A0001: El&aacute;stico confortable - S/ 89"></div>
                     </div>
                  </div>
               </div>
            </div>
			
         </div>
      </div>
   </section>
 -->
   <?php include 'php/common/footer.php';?>
   
   <script src="<?php echo $root_dir; ?>js/social-plugin.js"></script>
   
</body>
</html>