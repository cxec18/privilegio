<?php
$header_root = 0;
$menu_pages = array(
    array(0, "index.php", "Inicio"),
    array(0, "nosotros.php", "Nosotros"),
    array(1, "servicios.php", "Servicios"),
    array(0, "portafolio.php", "Portafolio"),
    array(0, "contactanos.php", "Cont&aacute;ctanos")
);

include 'php/common/header.php';
?>
    
    <script src="js/angular.min.js"></script>
   
	<section id="feature" data-ng-app="myApp">
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>Nuestros Servicios</h2>
                <p class="lead">La versatilidad es la mejor de nuestras cualidades.</p>
            </div>
            
            <div class="row" data-ng-controller="serviciosCtrl">
                <div class="features">
                     <div class="col-md-6 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" data-ng-repeat="x in servicios">
                        <div class="feature-wrap">
                            <i class="{{ x.icono }}"></i>
                            <h2>{{ x.titulo }}</h2>
                            <h3>{{ x.descripcion }}
                                <a data-ng-if="x.id==0 " href="<?php echo $root_dir;?>portafolio.php">Ver portafolio.</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="media contact-info wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="pull-left">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h2>&#191;Tienes alguna preguna&#63;</h2>
                            <p>Si deseas tener m&aacute;s informaci&oacute;n o tienes alguna duda sobre nuestros servicios puedes hacer <a href="contactanos.php">click aquí</a> para contactarnos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
   </section>
     
   <script type="text/javascript">
       var app = angular.module('myApp', []);
       app.controller('serviciosCtrl', function($scope, $http) {
          $http.get("json/sqlServicios.php")
          .then(function (response) {
             $scope.servicios = response.data.records;
          });
       });
   </script>
   
   <?php include $root_dir.'php/common/footer.php';?>
   
 </body>
</html>