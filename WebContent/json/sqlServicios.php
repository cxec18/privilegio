<?php
    include '../util/mysql/sqlconexion.php';
    
    $table_name = "Servicio";
    $table_fields = array("id", "icono", "titulo", "descripcion", "estado");
    $table_length = count($table_fields);
        
    $sql = "SELECT * FROM " . $table_name . " WHERE estado='A'";
	$result = $conn->query($sql);
	
	$outp = "";
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
		    if ($outp != "") {$outp .= ",";}
		    $outp .= '{';
		    for($x = 0; $x < $table_length; $x++) {
		       $outp .= '"'.$table_fields[$x].'"';
		       $outp .= ':';
		       $outp .= '"'.$row[$table_fields[$x]].'"';
		       if ($table_length-1 != $x){ $outp .= ",";} 
		    }
			$outp .= '}';
		}
	} else {
		echo "0 results";
	}
	
	$conn->close();
	$outp ='{"records":['.$outp.']}';
	
	echo ($outp);
?>