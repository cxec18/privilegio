<?php
    include '../util/mysql/sqlconexion.php';

    $table_name = "TipoProductos";
    $table_fields = array("id", "filtro", "titulo", "enlace", "imagen", "descripcion");
    $table_length = count($table_fields);
        
	$sql = 
	"SELECT  P.id AS id, TP.filtro AS filtro, P.titulo AS titulo, P.enlace AS enlace, CONCAT(IP.ubicacion, '/', IP.nombre) AS imagen, P.descripcion AS descripcion".
	" FROM Producto P INNER JOIN TipoProducto TP ON TP.id = P.idTipo INNER JOIN ImagenProducto IP ON P.id = IP.idProducto".
	" WHERE P.estado='A' AND TP.estado='A' AND IP.estado='A' AND IP.principal='Y'";

	$result = $conn->query($sql);
	
	$outp = "";
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
		    if ($outp != "") {$outp .= ",";}
		    $outp .= '{';
		    for($x = 0; $x < $table_length; $x++) {
		       $outp .= '"'.$table_fields[$x].'"';
		       $outp .= ':';
		       $outp .= '"'.$row[$table_fields[$x]].'"';
		       if ($table_length-1 != $x){ $outp .= ",";} 
		    }
			$outp .= '}';
		}
	} else {
		echo "0 results";
	}
	
	$conn->close();
	$outp ='{"records":['.$outp.']}';
	
	echo ($outp);
?>