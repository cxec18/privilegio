CREATE TABLE Servicio (
	id INT(2) PRIMARY KEY,
	icono VARCHAR(30) NOT NULL,
	titulo VARCHAR(30) NOT NULL,
	descripcion VARCHAR(150) NOT NULL,
	estado VARCHAR(1) NOT NULL
);
CREATE TABLE TipoProducto (
	id INT(2) PRIMARY KEY,
	filtro VARCHAR(15) NOT NULL,
	descripcion VARCHAR(15) NOT NULL,
	estado VARCHAR(1) NOT NULL
);
CREATE TABLE Producto (
	id INT(5) PRIMARY KEY,
	idTipo INT(2) NOT NULL,       -- FOREING KEY
	titulo VARCHAR(30) NOT NULL,
	enlace VARCHAR(30) NOT NULL,
	descripcion VARCHAR(150) NOT NULL,
	estado VARCHAR(1) NOT NULL
);
CREATE TABLE ImagenProducto (
	id INT(5) PRIMARY KEY,
	idProducto INT(5) NOT NULL,   -- FOREING KEY
	nombre VARCHAR(30) NOT NULL,
	ubicacion VARCHAR(30) NOT NULL,
	descripcion VARCHAR(15) NULL,
	principal VARCHAR(1) NOT NULL,
	estado VARCHAR(1) NOT NULL
);


INSERT INTO Servicio (id, icono, titulo, descripcion, estado) 
       VALUES (0, 'fa fa-line-chart', 'Venta de Outfit', 'La mejor calidad de nuestros productos al alcance de sus manos.', 'A');
INSERT INTO Servicio (id, icono, titulo, descripcion, estado) 
       VALUES (1, 'fa fa-magic', 'Asesoría de Moda', '¡Tómate un descanso!, nosotros nos encargaremos de hacerte lucir en cualquier ocasión.', 'A');
INSERT INTO Servicio (id, icono, titulo, descripcion, estado) 
       VALUES (2, 'fa fa-pencil', 'Diseño de Moda', 'Desarrollamos las mejores tendencias en moda con el toque de exclusividad que el mercado exige.', 'A');
INSERT INTO Servicio (id, icono, titulo, descripcion, estado) 
       VALUES (3, 'fa fa-thumbs-o-up', 'Confección', 'Los colaboradores con las más altas capacidades de producción están a su disposición.', 'A');

       
INSERT INTO TipoProducto (id, filtro, descripcion, estado) VALUES (1, 'camisas', 'Camisas', 'A');
INSERT INTO TipoProducto (id, filtro, descripcion, estado) VALUES (2, 'polos', 'Polos', 'A');
INSERT INTO TipoProducto (id, filtro, descripcion, estado) VALUES (3, 'pantalones', 'Pantalones', 'A');
INSERT INTO TipoProducto (id, filtro, descripcion, estado) VALUES (4, 'accesorios', 'Accesorios', 'A');


INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (0, 1, 'A0001', 'A0001.php', 'Estilo sport elegante: Camisa algodón strech.', 'A');

       
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado) 
	   VALUES (0, 0, 'IMG_3742.png', 'A0001', null, 'N', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (1, 0, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado) 
	   VALUES (2, 0, 'IMG_3779.png', 'A0001', null, 'N', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado) 
	   VALUES (3, 0, 'IMG_3840.png', 'A0001', null, 'N', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado) 
	   VALUES (4, 0, 'IMG_3932.png', 'A0001', null, 'N', 'A');

-------------------------------------------------------------------------------------------------
-- DATA DE PRUEBA
-------------------------------------------------------------------------------------------------
INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (1, 1, 'A0001', 'A0001.php', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'A');
INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (2, 1, 'A0001', 'A0001.php', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'A');
INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (3, 1, 'A0001', 'A0001.php', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'A');
INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (4, 1, 'A0001', 'A0001.php', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'A');
INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (5, 1, 'A0001', 'A0001.php', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'A');
INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (6, 1, 'A0001', 'A0001.php', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'A');
INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (7, 1, 'A0001', 'A0001.php', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'A');
INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (8, 1, 'A0001', 'A0001.php', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'A');
INSERT INTO Producto (id, idTipo, titulo, enlace, descripcion, estado) 
       VALUES (9, 1, 'A0001', 'A0001.php', 'There are many variations of passages of Lorem Ipsum available, but the majority', 'A');

INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (5, 1, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (6, 2, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (7, 3, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (8, 4, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (9, 5, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (10, 6, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (11, 7, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (12, 8, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
INSERT INTO ImagenProducto (id, idProducto, nombre, ubicacion, descripcion, principal, estado)  
	   VALUES (13, 9, 'IMG_3768.png', 'A0001', null, 'Y', 'A');
	   
--------------------------------------------------------------------
-- TRAE PRODUCTOS
--------------------------------------------------------------------
SELECT  P.id AS id,
        TP.filtro AS filtro,
        P.titulo AS titulo,
        P.enlace AS enlace,
        CONCAT(IP.ubicacion, '/', IP.nombre) AS imagen,
        P.descripcion AS descripcion
FROM    Producto P
        INNER JOIN TipoProducto TP ON TP.id = P.idTipo
        INNER JOIN ImagenProducto IP ON P.id = IP.idProducto
WHERE   P.estado='A' AND TP.estado='A' AND IP.estado='A' AND IP.principal='Y'
--------------------------------------------------------------------
	   
SELECT * FROM Servicio;
SELECT * FROM TipoProducto;
SELECT * FROM ImagenProducto;
SELECT * FROM Producto;
     
DELETE FROM Producto;

DROP TABLE Servicio;
DROP TABLE TipoProducto;
DROP TABLE Producto;
DROP TABLE ImagenProducto;
