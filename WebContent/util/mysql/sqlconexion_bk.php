<?php
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=ISO-8859-1");

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "privilegio";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 

	$sql = "SELECT Id, Descripcion, Estado FROM Pagina";
	$result = $conn->query($sql);
	
	$outp = "";
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"Id":"' . $row["Id"] . '",';
			$outp .= '"Descripcion":"'. $row["Descripcion"] . '",';
			$outp .= '"Estado":"'. $row["Estado"] . '"}';
		}
	} else {
		echo "0 results";
	}
	$conn->close();
    //$outp = "[" . $outp ."]";
	$outp ='{"records":['.$outp.']}';
	
	echo ($outp);
?>