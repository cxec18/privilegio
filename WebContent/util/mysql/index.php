<html xmlns="http://www.w3.org/1999/xhtml"><head><title>
	Lista Requerimientos Pendientes
</title>

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=8">
    <!--
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	-->
	
	<script type="text/javascript" src="angular.min.js"></script>
</head>
<body>
	<div ng-app="myApp" ng-controller="paginasCtrl">
	<table>
	  <tr ng-repeat="x in paginas">
		<td>{{ x.Id }}</td>
		<td>{{ x.Descripcion }}</td>
		<td>{{ x.Estado }}</td>
	  </tr>
	</table>

	</div>

	<script>
	var app = angular.module('myApp', []);
	app.controller('paginasCtrl', function($scope, $http) {
		$http.get("sqlconexion.php")
		.then(function (response) {
			$scope.paginas = response.data.records;
		});
	});
	</script>

</body>
</html>