(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'Messenger'));

$(function() {
	var app_id = '860714117353185';
    var scopes = 'email, user_friends, user_about_me, user_birthday, user_location ';
    var btn_login = '<a href="#" id="login" class="btn btn-primary">Iniciar Sesión</a>'
    var div_session = "<div id='facebook-session'>"+
    					"<strong></strong>"+
    					"<h4></h4>"+
    					"<h5></h5>"+
    					"<img>"+
    					"<a href='#' id='logout' class='btn btn-danger'>Cerrar Sessión</a>"+
    					"</div>";
    	
	window.fbAsyncInit = function() {
		FB.init({
		    appId      : app_id,
		    status     : true,
		    cookie     : true, 
		    xfbml      : true,
		    version    : 'v2.3'
		});

		FB.getLoginStatus(function(response) {
		    statusChangeCallback(response, function(){
		    	
		    });
		});
	};
    	
	var statusChangeCallback = function(response, callback) {
	    
	    if (response.status === 'connected') {
	    	getFacebookData();
	    } else {
	    	callback(false);
	    }
	}
        
    var checkLoginState = function(callback) {
	    FB.getLoginStatus(function(response) {
	      statusChangeCallback(response, function(data){
	    	 callback(data); 
	      });
	    });
	}
    
    var getFacebookData = function(){
    	FB.api('/me', function(response){
    		$('#login').after(div_session);
    		$('#login').remove();
    		$('#facebook-session strong').text("Bienvenido: "+response.name);
    		$('#facebook-session h4').text("Email: "+response.email);
    		$('#facebook-session h5').text("Fecha de Nacimiento: "+response.birthday);
    		$('#facebook-session img').attr('src','http://graph.facebook.com/'+response.id+'/picture?type=large');
    	})
    }
    
    var facebookLogin = function(){
    	checkLoginState(function(response){
    		if (!response){
    			FB.login(function(response){
    				if (response.status === 'connected')
    					getFacebookData();
    			}, {scope : scopes});
    		}
    	})
    }
    
    var facebookLogout = function(){
    	FB.getLoginStatus(function(response) {
    		if (response.status === 'connected'){
    			FB.logout(function(response) {
    				$('#facebook-session').before(btn_login);
    				$('#facebook-session').remove();
    			});
    		}
    	});
    }
    
    $(document).on('click', '#login', function(e){
    	e.preventDefault();
    	facebookLogin();
    })
    
    $(document).on('click', '#logout', function(e){
    	e.preventDefault();
    	if (confirm("¿Está seguro?"))
    		facebookLogout();
    	else
    		return false;
    })
})