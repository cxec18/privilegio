jQuery(function($) {'use strict',

	/*
	//#main-slider
	$(function(){
		$('#main-slider.carousel').carousel({
			interval: 8000
		});
	});*/


    $(".btn-circle").click(function(){
		var body = $("html, body");
		body.animate({scrollTop:0}, '700', 'swing');
    });

	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Initiat WOW JS
	new WOW().init();



	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),

			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
		});
	});

	
	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});	

	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});
	
    $('#nav-wrapper').height($("#nav").height());
    $('#nav').affix({
        offset: $('#nav').position()
    });

    /*
    setTimeout(function(){ 
        $(document).ready(function(){
        	$(".ip-header").css( "opacity", "0" );
        	$(".ip-header").css( "display", " none" );
        });
    }, 2000);
    */
    
    $(function() {

    	var app_id = '860714117353185';
    	var scopes = 'email, user_friends, user_online_presence';
    	
    	var btn_login = '<a href="#" id="login" class="btn btn-primary">Iniciar Sesión</a>'
    		
    	var div_session = "<div id='facebook-session'>"+
    						"<strong></strong>"+
    						"<img>"+
    						"<a href='#' id='logout' class='btn btn-danger'>Cerrar Sessión</a>"+
    						"</div>";
    	
    	window.fbAsyncInit = function() {
		  FB.init({
		    appId      : app-id,
		    status     : true,
		    cookie     : true, 
		    xfbml      : true,
		    version    : 'v2.2'
		  });

		  FB.getLoginStatus(function(response) {
		    statusChangeCallback(response, function(){
		    	
		    });
		  });

		  };
    })
    
    var statusChangeCallback = function(response, callback) {
	    console.log('statusChangeCallback');
	    console.log(response);
	    if (response.status === 'connected') {
	    	getFacebookData();
	    } else {
	    	callback(false);
	    }
	}
    
    var checkLoginState = function(callback) {
	    FB.getLoginStatus(function(response) {
	      statusChangeCallback(response, function(data){
	    	 callback(data); 
	      });
	    });
	}
    
    var getFacebookData = function(){
    	FB.api('/me', function(response){
    		$('#login').after(div_session);
    		$('#login').remove();
    		$('#facebook-session strong').text("Bienvenido: "+response.name);
    		$('#facebook-session img').attr('src','http://graph.facebook.com/'+response.id+'/picture?type=large');
    	})
    }
    
    var facebookLogin = function(){
    	checkLoginState(function(response){
    		if (!response){
    			FB.login(function(response){
    				if (response.status === 'connected')
    					getFacebookData();
    			}, {scope : scopes});
    		}
    	})
    }
    
    var facebookLogout = function(){
    	FB.getLoginStatus(function(response) {
    		if (response.status === 'connected'){
    			FB.logout(function(response) {
    				$('#facebook-session').before(btn_login);
    				$('#facebook-session').remove();
    			});
    		}
    	});
    }
    
    $(document).on('click', '#login', function(e){
    	e.prevenDefault();
    	facebookLogin();
    })
    
    $(document).on('click', '#logout', function(e){
    	e.prevenDefault();
    	if (confirm("¿Está seguro?"))
    		facebookLogout();
    	else
    		return false;
    })
});